# README #

### Creating the build environment ###

Install Docker and start the Docker service.

To create the build environment, at the top level in this repository say

`docker build --no-cache -t trinnity-benchmarks -f Dockerfile .`

You may need to give your user permission to use docker with

`gpasswd -a $USER docker`

The use of the `--no-cache` option to Docker causes the image to be fully
rebuilt. Unfortunately, Docker does not yet provide a way for the user to
supply the caching predicate for rules, so using `--no-cache` globally is the
only way to ensure that the image contains up-to-date components if, for
example, any of the contents change in a git repository .

### Running the benchmarks ###

To get a shell in the build environment, say

`docker run -i -t trinnity-benchmarks /bin/bash`

Once the shell has started, you can run benchmarking flows using `make`.

### x86\_64 evaluation ###

Say `touch Makefile.config && make end-to-end-cgo-x86_64`

Graphs should be produced in `build/graphs/networks/` and
`build/graphs-multithreaded/networks/`

### aarch64 evaluation ###

Build the cost model microbenchmarks by saying

`touch Makefile.config && make end-to-end-cgo-aarch64-build-cost-model-progs`

Once the microbenchmarks have built, copy the entire `trinnity-benchmarks`
directory to your aarch64 machine. On your aarch64 machine, say `make
end-to-end-cgo-aarch64-build-cost-model`.

Once the cost model has built, copy the entire `trinnity-benchmarks` directory
back to the Docker image. In the Docker shell, say `make
end-to-end-cgo-aarch64-networks-build`.

Once the network benchmarks have built, copy the entire `trinnity-benchmarks`
directory to your aarch64 machine. On your aarch64 machine, say `make
end-to-end-cgo-aarch64-networks-run`.

### Skip microbenchmarking ###

To speed up the evaluation process, we have bundled generated cost models for
several different CPUs. To use one of these, you can `git clone` this
repository inside the Docker shell, and then copy the relevant subdirectory
into `build/stats/` or `build/stats-multithreaded/` in the
`trinnity-benchmarks` directory.

Before copying the bundled cost model, please say `make clean` in the
`trinnity-benchmarks` repository. The `make clean` is important, because some
temporary files may exist from earlier attempts to build the cost model, which
`make` will not regenerate, as they appear up-to-date. If errors persist, you
can manually delete `build/layouts.csv` before continuing.

For example, to use the bundled cost model for the Intel Core i5-4570 in
single-threaded mode, say `mkdir -p trinnity-benchmarks/build/stats/ && cp
cgo-ae-2018/cost-models/core-i5-4570/single-threaded/*.csv
trinnity-benchmarks/build/stats/`.

You can then build and run the network benchmarks by changing to the
`trinnity-benchmarks` directory and saying `touch
Makefile.config && make end-to-end-cgo-x86_64-networks`

To use the bundled cost model for aarch64, copy the model data into the
appropriate benchmark directories and start from the `make
end-to-end-cgo-aarch64-networks-build` step above.
