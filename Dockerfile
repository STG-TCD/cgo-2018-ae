FROM base/archlinux

MAINTAINER Andrew Anderson

#RUN pacman-key --refresh-keys

RUN pacman --quiet --noconfirm -Syu

# Install software dependencies

RUN pacman --quiet --noconfirm -S git aarch64-linux-gnu-gcc doxygen ghc cabal-install lzop numactl texlive-core wget sudo patch make fakeroot

# Install epstool

WORKDIR /tmp

RUN git clone https://aur.archlinux.org/epstool.git

RUN chmod -R a+rw /tmp/epstool

USER nobody

WORKDIR /tmp/epstool

RUN makepkg -s

USER root

RUN pacman --noconfirm -U epstool*.xz

WORKDIR /

# Install gnuplot-headless

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S lua ncurses readline

RUN git clone https://aur.archlinux.org/gnuplot-headless.git

RUN chmod -R a+rw /tmp/gnuplot-headless

USER nobody

WORKDIR /tmp/gnuplot-headless

RUN makepkg -s

USER root

RUN pacman --noconfirm -U gnuplot-headless*.xz

WORKDIR /

# Install openblas-lapack

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S gcc-libs gcc-fortran perl

RUN git clone https://aur.archlinux.org/openblas-lapack.git

RUN chmod -R a+rw /tmp/openblas-lapack

USER nobody

WORKDIR /tmp/openblas-lapack

RUN makepkg -s

USER root

RUN pacman --noconfirm -U openblas-lapack*.xz

WORKDIR /

# Install python-pyamg

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S python python-setuptools

RUN git clone https://aur.archlinux.org/python-pyamg.git

RUN chmod -R a+rw /tmp/python-pyamg

USER nobody

WORKDIR /tmp/python-pyamg

RUN makepkg -s

USER root

RUN pacman --noconfirm -U python-pyamg*.xz

WORKDIR /

# Install python2-pywavelets

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S python2 python2-numpy cython cython2 python-numpy python-setuptools python2-numpy python2-setuptools

RUN git clone https://aur.archlinux.org/python2-pywavelets.git

RUN chmod -R a+rw /tmp/python2-pywavelets

USER nobody

WORKDIR /tmp/python2-pywavelets

RUN makepkg -s

USER root

RUN pacman --noconfirm -U python2-pywavelets*.xz

WORKDIR /

# Install python-pywavelets

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S python python-numpy cython cython2 python-numpy python-setuptools python2-numpy python2-setuptools

RUN git clone https://aur.archlinux.org/python-pywavelets.git

RUN chmod -R a+rw /tmp/python-pywavelets

USER nobody

WORKDIR /tmp/python-pywavelets

RUN makepkg -s

USER root

RUN pacman --noconfirm -U python-pywavelets*.xz

WORKDIR /

# Install python-scikit-image

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S boost boost-libs cython gflags google-glog \
hdf5 ipython leveldb lmdb opencv protobuf python python-dateutil python-gflags \
python-h5py python-matplotlib python-networkx python-nose python-numpy \
python-pandas python-pillow python-protobuf python-scipy python-six python-yaml \
python-matplotlib python-networkx python-pillow python-scipy cython cython2 python-matplotlib \
python-networkx python-pillow python-scipy python-six python2-matplotlib \
python2-networkx python2-pillow python2-scipy python2-six freeimage python-pyqt4

RUN git clone https://aur.archlinux.org/python-scikit-image.git

RUN chmod -R a+rw /tmp/python-scikit-image

USER nobody

WORKDIR /tmp/python-scikit-image

RUN makepkg -s

USER root

RUN pacman --noconfirm -U python-scikit-image*.xz

WORKDIR /

# Install python-pydotplus

WORKDIR /tmp

RUN git clone https://aur.archlinux.org/python-pydotplus.git

RUN chmod -R a+rw /tmp/python-pydotplus

USER nobody

WORKDIR /tmp/python-pydotplus

RUN makepkg -s

USER root

RUN pacman --noconfirm -U python-pydotplus*.xz

WORKDIR /

# Install python-leveldb

WORKDIR /tmp

RUN git clone https://aur.archlinux.org/python-leveldb.git

RUN chmod -R a+rw /tmp/python-leveldb

USER nobody

WORKDIR /tmp/python-leveldb

RUN makepkg -s

USER root

RUN pacman --noconfirm -U python-leveldb*.xz

WORKDIR /

# Install caffe-cpu

WORKDIR /tmp

RUN git clone https://aur.archlinux.org/caffe-cpu.git

RUN chmod -R a+rw /tmp/caffe-cpu

USER nobody

WORKDIR /tmp/caffe-cpu

RUN makepkg -s

USER root

RUN pacman --noconfirm -U caffe-cpu*.xz

WORKDIR /

# Install pbqp

WORKDIR /tmp

RUN git clone https://aur.archlinux.org/pbqp.git

RUN chmod -R a+rw /tmp/pbqp

USER nobody

WORKDIR /tmp/pbqp

RUN makepkg -s

USER root

RUN pacman --noconfirm -U pbqp*.xz

WORKDIR /

# Install mkl-dnn

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S cmake

RUN git clone https://aur.archlinux.org/mkl-dnn.git

RUN chmod -R a+rw /tmp/mkl-dnn

USER nobody

WORKDIR /tmp/mkl-dnn

RUN makepkg -s

USER root

RUN pacman --noconfirm -U mkl-dnn*.xz

WORKDIR /

# Install openblas-aarch64 for cross compilation

WORKDIR /tmp

RUN git clone https://github.com/xianyi/OpenBLAS.git

WORKDIR /tmp/OpenBLAS/

RUN git checkout v0.2.20

RUN make BINARY=64 CC=aarch64-linux-gnu-gcc HOSTCC=gcc NOFORTRAN=1 NOSHARED=1 TARGET=CORTEXA57 libs

RUN mkdir -p /root/Tools/openblas-aarch64/lib/

RUN cp -f libopenblas* /root/Tools/openblas-aarch64/lib/

RUN ln -sf /root/Tools/openblas-aarch64/lib/libopenblas_* /root/Tools/openblas-aarch64/lib/libopenblas.a

RUN mkdir -p /root/Tools/openblas-aarch64/include/

RUN cp -rf *.h /root/Tools/openblas-aarch64/include/

WORKDIR /

# Download and install the library

WORKDIR /tmp

RUN git clone https://bitbucket.org/STG-TCD/trinnity.git

WORKDIR /tmp/trinnity

RUN git checkout cgo-ae

RUN mkdir -p doc

RUN chmod -R a+rw .

USER nobody

RUN make -B doc

RUN make -B package

User root

RUN pacman --noconfirm -U triNNity*.xz

# Download the benchmark suite

WORKDIR /tmp

RUN pacman --quiet --noconfirm -S bind-tools

RUN cabal update

RUN git clone https://bitbucket.org/STG-TCD/trinnity-benchmarks.git

WORKDIR /tmp/trinnity-benchmarks

RUN git checkout cgo-ae

# Install bc

RUN pacman --quiet --noconfirm -S bc
